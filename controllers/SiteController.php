<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['books', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->login(); 
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogin()
    {
        $weather = $this->actionWeather();
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['books/index']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
            'weather' => $weather
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['site/login']);
    }

    /**
     * Displays wather
     *
     * @return Response|string
     */
    public static function actionWeather()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.hgbrasil.com/weather?key=SUA-CHAVE&city_name=Campinas,SP'
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response, true);        
        $weather = array(
            'condition_code' => $response['results']['condition_code'],
            'description'=> $response['results']['description'],
            'condition_slug'=> $response['results']['condition_slug'],
        );
        
        return $weather;
    }
}
