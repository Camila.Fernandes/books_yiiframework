<?php

namespace app\controllers;

use Yii;
use app\models\Books;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class BooksController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $books = Books::find()
        ->where(['is', 'deleted_at', null])
        ->all();

        return $this->render('index', [
            'books' => $books
        ]);
    }

    public function actionCreate()
    {
        $request = \yii::$app->request;
        
        if($request->isPost) {
           
            $model = new Books();
            $model->attributes =  $request->post(); 
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();      
  
            return $this->redirect(['books/index']);
        }

        return $this->render('create');
    }

    public function actionUpdate($id)
    {
        $model = Books::findOne($id);

        if(!$model) {
            throw new NotFoundHttpException("Página não encontrada");
        }

        $request = \yii::$app->request;
        if ($request->isPost) {
            $model->attributes =  $request->post(); 
            $model->updated_at = date('Y-m-d H:i:s');
            $model->save();
        
            return $this->redirect(['books/index']);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionDelete($id)
    {
        $model = Books::findOne($id);
        if(!$model) {
            throw new NotFoundHttpException("Página não encontrada");
        }

        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        return $this->redirect(['books/index']);
    }
}
