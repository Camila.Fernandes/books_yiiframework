<?php
    /* @var $this yii\web\View */
    use yii\helpers\Url;
    
    ?>
    <h1>New book</h1>


    <form name="form" method="post" action="<?= Url::to(['books/create']); ?>">

    <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" 
                value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="form-group">
            <label for="title">Titlr:</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="title">
        </div>
        <div class="form-group">
            <label for="author">Author:</label>
            <input type="text" class="form-control" id="author" name="author" placeholder="Author">
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Description">
        </div>
        <div class="form-group">
            <label for="pages">Pages:</label>
            <input type="tpagesxt" class="form-control" id="pages" name="pages" placeholder="Pages">
        </div>
       
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>