<?php
    /* @var $this yii\web\View */
    use yii\helpers\Url;
    
    ?>
    <h1>New book</h1>


    <form name="form" method="post" action="<?= Url::to(['books/update', 'id' => $model->id]); ?>">

    <input type="hidden" name="<?= \yii::$app->request->csrfParam; ?>" 
                value="<?= \yii::$app->request->csrfToken; ?>">

        <div class="form-group">
            <label for="title">Titlr:</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="title" value="<?= $model->title; ?>">
        </div>
        <div class="form-group">
            <label for="author">Author:</label>
            <input type="text" class="form-control" id="author" name="author" placeholder="Author" value="<?= $model->author; ?>">
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?= $model->description; ?>">
        </div>
        <div class="form-group">
            <label for="pages">Pages:</label>
            <input type="tpagesxt" class="form-control" id="pages" name="pages" placeholder="Pages" value="<?= $model->pages; ?>">
        </div>
       
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>