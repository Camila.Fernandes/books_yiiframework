<?php
    use yii\helpers\Url;
    /* @var $this yii\web\View */
    ?>
    <h1 class="text text-center">Books</h1>
    <a href="<?= Url::to(['books/create']);?>" class="btn btn-success">New book</a>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th>Author</th>
            <th>Pages</th>
            <th>Create At</th>
            <th>-</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($books as $book): ?>
                <tr>
                    <td><?= $book->id; ?></td>
                    <td><?= $book->title; ?></td>
                    <td><?= $book->description; ?></td>
                    <td><?= $book->author; ?></td>
                    <td><?= $book->pages; ?></td>
                    <td><?= $book->created_at; ?></td>
                    <td>
                        <a href="<?= Url::to(['books/update', 'id' => $book->id]);?>">Edit </a> | 
                        <a href="<?= Url::to(['books/delete', 'id' => $book->id]);?>">Delete </a> 
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>