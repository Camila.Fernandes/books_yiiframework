## Installation

Fazer o clone do projeto

Instalar as dependências

```bash
composer install
```
Modificar o arquivo config\db.php com as credentials de banco

```
'class' => 'yii\db\Connection',
'dsn' => 'mysql:host=localhost;dbname=dnmane',
'username' => 'user',
'password' => 'password',
'charset' => 'utf8',

```

Rodar a migration para criação das tabelas

```bash

php yii migrate

```

Rodar o server

```bash

php yii serve

```

